#include<stdio.h>
void fibonacciSeq(int no);

int main()
{
	int no;
	printf("\nEnter an integer : ");
	scanf("%d", &no);
	printf("\n");
	fibonacciSeq(no);
	printf("\n");
	return 0;
}

void fibonacciSeq(int no)
{
	if(no == 0)
	{
		printf("0\n");
	}
	else if(no == 1)
	{
		printf("1\n");
	}
	else
	{
		int x = 0, y = 1, fib = 0;
		for(int z = 0 ; z <= no ; z++)
		{
			printf("%d\n", x);
			fib = x + y;
			x = y;
			y = fib;
		}
	}
}
