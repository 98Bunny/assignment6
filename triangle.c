#include<stdio.h>
void pattern(int no);

int main()
{
	int no;
	printf("\nEnter an integer : ");
	scanf("%d", &no);
	printf("\n");	
	pattern(no);
	printf("\n");	
	return 0;
}

void pattern(int no)
{
	for(int x = 1 ; x <= no ; x++)
	{
		for(int y = 1 ; y <= x ; y++)
		{
			printf("%d", y);
		}
		printf("\n");
	}
}
